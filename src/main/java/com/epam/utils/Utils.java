package com.epam.utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Utils {
    public static Logger logger = LogManager.getLogger(Utils.class);
    private static List<String> startMenu;
    private static List<String> sprintMenu;
    private static List<String> actionMenu;
    public static final String PRODUCT_BACKLOG = "Product Backlog";
    public static final String SPRINT_BACKLOG = "Sprint Backlog";
    public static final String SPRINT_TASKS = "Sprint Backlog";
    public static final String IN_PROGRESS = "In progress";
    public static final String REVIEW = "Review";
    public static final String TEST = "Test";
    public static final String DONE = "Done";
    public static final String BLOCKED = "Blocked";
    public static final String FINAL_STAGE = "This is final stage";
    public static final String MAIN_MENU_TITLE = "Project Control Menu\n";
    public static final String CHOICE_MENU_MSG = "Choose menu item: ";
    public static final String INP_CONS_ERR = "Error while reading from console";
    public static final String NO_ITEM_ERR = "No such menu item\n";
    public static final String EMPTY_LIST = "List is empty";
    public static final String TASK_FORMAT = "Task %d%nNow in %s stage%n";
    public static final String STAGE_TASK_FORMAT = "%15s%2d%n";
    public static final String BACKLOG_ADD_MSG = "\n[a]dd task or [e]xit: ";
    public static final String SPRINT_ADD_MSG = "\n[a]dd task to sprint, [p]roceed, [e]xit: ";
    public static final String CHOOSE_TASK_NUMBER = "\nChoose task number: ";
    public static final String WRONG_ACTION = "Can not perform chosen action\n";

    static {
        startMenu = new LinkedList<>();
        startMenu.add("1. Add task to Product BackLog.");
        startMenu.add("2. Start sprint");
        startMenu.add("0. Exit program");
        actionMenu = new ArrayList<>();
        actionMenu.add("Add task to [S]print Backlog");
        actionMenu.add("[P]rocess task");
        actionMenu.add("{R}eview Task");
        actionMenu.add("[T]est task");
        actionMenu.add("[C]omplete task");
        actionMenu.add("{B}lock task");
        actionMenu.add("Nothing can be done. Task already completed");
        actionMenu.add("Task is blocked, nothing can be done");
        actionMenu.add("Not in sprint. Can not control the task");
    }

    private Utils() {}

    public static Supplier<Stream<String>> getStartMenu() {
        return () -> startMenu.stream();
    }

    public static List<String> getActMenu() {
      return actionMenu;
    }
}
