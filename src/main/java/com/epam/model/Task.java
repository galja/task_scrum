package com.epam.model;

import com.epam.model.stages.ProductBacklogStage;
import com.epam.model.stages.ProjectStage;
import com.epam.view.Performable;
import java.util.LinkedHashMap;
import java.util.Map;
import static com.epam.utils.Utils.*;

public class Task {
    private static int counter = 0;
    private ProjectStage stage;
    private final int nameOrder = ++counter;
    private Map<String, Performable> actions;

    public Task(){
        stage = new ProductBacklogStage();
        actions = prepareActions();
    }

    private Map<String, Performable> prepareActions() {
        actions = new LinkedHashMap<>();
        actions.put("s", () -> stage.moveToSprintBacklogStage(this));
        actions.put("p", () -> stage.moveToProgressStage(this));
        actions.put("r", () -> stage.moveToReviewStage(this));
        actions.put("t", () -> stage.moveToTestStage(this));
        actions.put("c", () -> stage.moveToDoneStage(this));
        actions.put("b", () -> stage.moveToBlockedStage(this));
        return actions;
    }

    public void processChoice(String ch) {
        actions
            .entrySet()
            .stream()
            .filter(a -> a.getKey().equals(ch))
            .forEach(a -> a.getValue().perform());
    }

    public void setStage(ProjectStage stage){
        this.stage = stage;
    }

    public ProjectStage getStage() {
        return stage;
    }

    public int getNameOrder() {
        return nameOrder;
    }


    @Override
    public String toString() {
        return String.format(TASK_FORMAT, getNameOrder(), getStage()) +
                                                                    getStage().showStageActions();
    }
}
