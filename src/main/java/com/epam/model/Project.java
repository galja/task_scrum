package com.epam.model;

import com.epam.model.stages.*;
import com.epam.utils.Utils;
import static com.epam.utils.Utils.*;

import java.util.*;

public class Project {
    private List<Task> backLogTasks;
    private List<ProjectStage> stages;
    private Map<Integer, Task> runningTasks;
    private List<String> projStajes;

    public Project() {
        runningTasks = new LinkedHashMap<>();
        projStajes = prepareProjStages();
        backLogTasks = new LinkedList<>();
    }

// we could have an array of Tasks from control
  public void fillBackLog(Task input) {
    backLogTasks.add(input);
  }
  
    public void fillBackLog(List<Task> input) {
        backLogTasks = new LinkedList<>();
        backLogTasks.addAll(input);
    }

    public void addTaskToBacklog() {
        backLogTasks.add(new Task());
    }

    public void fillStagesList() {
        stages = new LinkedList<>();
        stages.add(new BlockedStage());
        stages.add(new DoneStage());
        stages.add(new ProgressStage());
        stages.add(new ReviewStage());
        stages.add(new SprintBacklogStage());
        stages.add(new TestStage());
    }

    //  dont know how it has to be with that meth
    public void addTaskToSprint(int taskNumber) {
        List<Task> tasksToRemove = new ArrayList<>();
        for (Task task : backLogTasks) {
            if(task.getNameOrder()==taskNumber){
                task.setStage(new SprintBacklogStage());
                runningTasks.put(taskNumber, task);
                tasksToRemove.add(task);
            }
        }
        backLogTasks.removeAll(tasksToRemove);
    }

    private List<String> prepareProjStages() {
        List<String> stages = new LinkedList<>();
        stages.add(Utils.SPRINT_BACKLOG);
        stages.add(Utils.IN_PROGRESS);
        stages.add(Utils.REVIEW);
        stages.add(Utils.TEST);
        stages.add(Utils.DONE);
        stages.add(Utils.BLOCKED);
        return stages;
    }

    public List<Task> getBackLog() {
        return backLogTasks;
    }

    public void stopSprintOperation(){
        for (Map.Entry<Integer, Task> entry: runningTasks.entrySet()) {
            if((!entry.getValue().getStage().getStageName().equals(BLOCKED))&
                    (!entry.getValue().getStage().getStageName().equals(DONE))){
                backLogTasks.add(entry.getValue());
            }
        }
        runningTasks.clear();
    }

    public String viewProjectState() {
      StringBuilder output = new StringBuilder();
      for(String stName : projStajes) {
        output.append(stName);
        output.append( "\n");
          runningTasks
              .entrySet()
              .stream()
              .filter(t -> t.getValue().getStage().getStageName().equals(stName))
              .map(t -> String.format(Utils.STAGE_TASK_FORMAT, "Task", t.getValue().getNameOrder()))
              .forEach(output::append);

      }
      return output.toString();
    }

    public Task getTaskById(int id) {
      return runningTasks.get(id);
    }

    public Map<Integer, Task> getRunningTasks() {
        return runningTasks;
    }
}
