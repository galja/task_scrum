package com.epam.model.stages;

import com.epam.model.Task;
import com.epam.utils.Utils;
import java.util.List;

public abstract class ProjectStage {
  String stageName;
  int[] actionCode;

  public ProjectStage() {
  }

  public void moveToSprintBacklogStage(Task task) {
    operationIsNotAllowed(Utils.SPRINT_BACKLOG);
  }

  public void moveToProgressStage(Task task){
    operationIsNotAllowed(Utils.IN_PROGRESS);
  }

  public void moveToReviewStage(Task task){
    operationIsNotAllowed(Utils.REVIEW);
  }

  public void moveToTestStage(Task task){
    operationIsNotAllowed(Utils.TEST);
  }

  public void moveToDoneStage(Task task){
    operationIsNotAllowed(Utils.DONE);
  }

  public void moveToBlockedStage(Task task){
    operationIsNotAllowed(Utils.BLOCKED);
  }

  private void operationIsNotAllowed(String stageName) {
    Utils.logger.info("Moving task to %1 is not allowed" + stageName);
  }

  public abstract List<String> createMenu();

  public String getStageName() {
    return stageName;
  }

  public String toString() {
    return getStageName();
  }

  public String showStageActions() {
    String menu = "";
    for(int i = 0; i < actionCode.length; i++) {
      menu += Utils.getActMenu().get(actionCode[i]) + "\n";
    }
    return menu;
  }
}
