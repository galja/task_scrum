package com.epam.model.stages;

import com.epam.model.Task;
import com.epam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class TestStage extends ProjectStage {

    public TestStage() {
        stageName = Utils.TEST;
        actionCode = new int[]{4, 5, 1};
    }

    @Override
    public void moveToProgressStage(Task task) {
        task.setStage(new ReviewStage());
    }

    @Override
    public void moveToBlockedStage(Task task) {
        task.setStage(new BlockedStage());
    }

    @Override
    public List<String> createMenu() {
        List<String> menu = new ArrayList<>();
        menu.add(Utils.IN_PROGRESS);
        menu.add(Utils.BLOCKED);
        menu.add(Utils.DONE);
        return menu;
    }

    @Override
    public void moveToDoneStage(Task task) {
        task.setStage(new BlockedStage());
    }
}
