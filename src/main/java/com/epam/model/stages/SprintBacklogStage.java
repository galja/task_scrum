package com.epam.model.stages;

import com.epam.model.Task;
import com.epam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SprintBacklogStage extends ProjectStage {

    public SprintBacklogStage() {
        stageName = Utils.SPRINT_BACKLOG;
        actionCode = new int[] {1, 5};
    }

    @Override
    public void moveToProgressStage(Task task){
        task.setStage(new ProgressStage());
    }

    @Override
    public void moveToBlockedStage(Task task){
        task.setStage(new BlockedStage());
    }

    @Override
    public List<String> createMenu() {
        List<String> menu = new ArrayList<>();
        menu.add(Utils.IN_PROGRESS);
        menu.add(Utils.BLOCKED);
        return menu;
    }
}
