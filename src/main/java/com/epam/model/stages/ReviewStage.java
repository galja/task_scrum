package com.epam.model.stages;

import com.epam.model.Task;
import com.epam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ReviewStage extends ProjectStage {

    public ReviewStage() {
        stageName = Utils.REVIEW;
        actionCode = new int[]{3, 4, 5};
    }

    @Override
    public void moveToProgressStage(Task task){
        task.setStage(new ProgressStage());
    }

    @Override
    public void moveToBlockedStage(Task task){
        task.setStage(new BlockedStage());
    }

    @Override
    public void moveToTestStage(Task task){
        task.setStage(new TestStage());
    }

    @Override
    public List<String> createMenu() {
        List<String> menu = new ArrayList<>();
        menu.add(Utils.IN_PROGRESS);
        menu.add(Utils.TEST);
        menu.add(Utils.BLOCKED);
        return menu;
    }
}
