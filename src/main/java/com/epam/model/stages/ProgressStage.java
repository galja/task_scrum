package com.epam.model.stages;

import com.epam.model.Task;
import com.epam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProgressStage extends ProjectStage {

    public ProgressStage() {
        stageName = Utils.IN_PROGRESS;
        actionCode = new int[]{0, 2, 5};
    }

    @Override
    public void moveToSprintBacklogStage(Task task) {
        task.setStage(new SprintBacklogStage());
    }

    @Override
    public void moveToReviewStage(Task task) {
        task.setStage(new ReviewStage());
    }

    @Override
    public void moveToBlockedStage(Task task) {
        task.setStage(new BlockedStage());
    }

    @Override
    public List<String> createMenu() {
        List<String> menu = new ArrayList<>();
        menu.add(Utils.SPRINT_BACKLOG);
        menu.add(Utils.REVIEW);
        menu.add(Utils.BLOCKED);
        return menu;
    }
}
