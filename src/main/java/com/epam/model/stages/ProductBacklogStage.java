package com.epam.model.stages;

import com.epam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProductBacklogStage extends ProjectStage {

    public ProductBacklogStage() {
        stageName = Utils.PRODUCT_BACKLOG;
        actionCode = new int[]{8};
    }
    @Override
    public List<String> createMenu() {
        List<String> menu = new ArrayList<>();
        menu.add(Utils.SPRINT_BACKLOG);
        return menu;
    }

}
