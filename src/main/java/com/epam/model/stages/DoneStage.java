package com.epam.model.stages;

import com.epam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class DoneStage extends ProjectStage {

    public DoneStage() {
        stageName = Utils.DONE;
        actionCode = new int[]{6};
    }
    @Override
    public List<String> createMenu() {
        List<String> menu = new ArrayList<>();
        menu.add(Utils.FINAL_STAGE);
        return menu;
    }
}
