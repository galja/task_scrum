package com.epam.model.stages;

import com.epam.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class BlockedStage extends ProjectStage {

    public BlockedStage() {
        stageName = Utils.BLOCKED;
        actionCode = new int[]{7};
    }
    @Override
    public List<String> createMenu() {
        List<String> menu = new ArrayList<>();
        menu.add(Utils.FINAL_STAGE);
        return menu;
    }
}
