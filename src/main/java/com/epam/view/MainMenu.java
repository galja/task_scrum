package com.epam.view;

import com.epam.control.ProjectFlow;
import static com.epam.utils.Utils.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu {
  private ProjectFlow control;
  private BufferedReader br;
  private Map<Integer, Performable> exeStartMenu;

  public MainMenu() {
    control = new ProjectFlow();
    control.setMonitor(new StdView());
    br = new BufferedReader(new InputStreamReader(System.in));
    exeStartMenu = prepareExeMenu();
  }

  public void getChoice() {
    int menuBound = exeStartMenu.size() - 1;
    showStartMenu();
    try {
      String choice = br.readLine();
      if(choice.trim().matches("[0-" + menuBound + "]")) {
        processChoice(Integer.parseInt(choice));
      } else {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      logger.error(INP_CONS_ERR);
    } catch (IllegalArgumentException e) {
      logger.warn(NO_ITEM_ERR);
      getChoice();
    }
  }

  private void processChoice(int ch) {
    exeStartMenu
        .entrySet()
        .stream()
        .filter(i -> i.getKey() == ch)
        .forEach(i -> i.getValue().perform());
    getChoice();
  }

  private void showStartMenu() {
    logger.info(MAIN_MENU_TITLE);
    getStartMenu()
        .get()
        .map(i -> i + "\n")
        .forEach(logger::info);
    logger.info(CHOICE_MENU_MSG);
  }

  private Map<Integer, Performable> prepareExeMenu() {
    Map<Integer, Performable> menu = new LinkedHashMap<>();
    menu.put(0, control::ExitApp);
    menu.put(1, control::addTaskToBackLog);
    menu.put(2, control::startSprint);
    return menu;
  }
}
