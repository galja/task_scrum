package com.epam.view;

@FunctionalInterface
public interface Performable {
  void perform();
}
