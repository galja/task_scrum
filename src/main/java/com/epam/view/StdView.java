package com.epam.view;

import static com.epam.utils.Utils.*;

public class StdView {

  public void display(String data) {
    logger.info(data);
  }

}
