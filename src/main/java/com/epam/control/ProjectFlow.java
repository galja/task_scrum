package com.epam.control;

import com.epam.model.Project;
import com.epam.model.Task;

import static com.epam.utils.Utils.*;

import com.epam.utils.Utils;
import com.epam.view.StdView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ProjectFlow {
    private Project currentProject;
    private BufferedReader br;
    private StdView monitor;

    public ProjectFlow() {
        currentProject = new Project();
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    public void setMonitor(StdView monitor) {
        this.monitor = monitor;
    }

    public void ExitApp() {
        System.exit(0);
    }

    private StringBuilder showBackLog(List<Task> bkLog) {
        StringBuilder out = new StringBuilder();
        bkLog
                .stream()
                .map(t -> String.format(Utils.STAGE_TASK_FORMAT, "Task", t.getNameOrder()))
                .forEach(out::append);
        return out;
    }

    public void addTaskToBackLog() {
        String choice = "";
        StringBuilder output = new StringBuilder(Utils.PRODUCT_BACKLOG + " :\n");

        List<Task> bkLog = currentProject.getBackLog();
        if (bkLog.size() > 0) {
            output.append(showBackLog(bkLog));
        }
        monitor.display(output.toString());
        monitor.display(BACKLOG_ADD_MSG);
        try {
            choice = br.readLine();
            if (!choice.toLowerCase().trim().matches("a|e")) {
                throw new IllegalArgumentException();
            }
        } catch (IOException e) {
            Utils.logger.error(Utils.INP_CONS_ERR);
        } catch (IllegalArgumentException e) {
            Utils.logger.warn(Utils.WRONG_ACTION);
            addTaskToBackLog();
        }
        switch (choice) {
            case "a":
                currentProject.fillBackLog(new Task());
                addTaskToBackLog();
                break;
            case "e":
                return;
            default:
                break;
        }
    }

    public void startSprint() {
        // algorithm for starting a sprint
        //check if is any tasks in backlog
        // call sprint menu from main menu at the end;
        if (!currentProject.getBackLog().isEmpty()) {
            String choice = "";
            StringBuilder output = new StringBuilder(Utils.PRODUCT_BACKLOG + " :\n");

            output.append(showBackLog(currentProject.getBackLog()));
            monitor.display(output.toString());
            monitor.display(SPRINT_ADD_MSG);
            try {
                choice = br.readLine();
                if (!choice.toLowerCase().trim().matches("a|e|p")) {
                    throw new IllegalArgumentException();
                }
            } catch (IOException e) {
                Utils.logger.error(Utils.INP_CONS_ERR);
            } catch (IllegalArgumentException e) {
                Utils.logger.warn(Utils.WRONG_ACTION);
                startSprint();
            }
            switch (choice) {
                case "a":
                    monitor.display(CHOOSE_TASK_NUMBER);
                    addTaskToSprint();
                    startSprint();
                    break;
                case "p":
                    chooseTaskState();
                    break;
                case "e":
                    return;
                default:
                    break;
            }
//      currentProject.fillBackLog(backlogTasks);

        } else {
            logger.warn(EMPTY_LIST);
        }
    }

    private void chooseTaskState() {
        try{
            monitor.display(currentProject.viewProjectState());
            monitor.display(CHOOSE_TASK_NUMBER);
            Task curr = currentProject.getTaskById(scanInteger());
            monitor.display(curr.toString());
            String choice = "";
            try {
                choice = br.readLine();
                if (!choice.toLowerCase().trim().matches("s|r|p|t|c|b")) {
                    throw new IllegalArgumentException();
                }
                curr.processChoice(choice);
                chooseTaskState();
            } catch (IOException e) {
                Utils.logger.error(Utils.INP_CONS_ERR);
            } catch (IllegalArgumentException e) {
                Utils.logger.warn(Utils.WRONG_ACTION);
                if(choice.equals("q")){
                    stopSprint();
                }
                chooseTaskState();
            }

        }catch (IllegalArgumentException e) {
            Utils.logger.warn(Utils.WRONG_ACTION);
            chooseTaskState();
        }


    }

    private int scanInteger() {
        String choice = "";
        try {
            choice = br.readLine();
            if (!choice.trim().matches("[1-9][0-9]*")) {
                throw new IllegalArgumentException();
            }
        } catch (IOException e) {
            Utils.logger.error(Utils.INP_CONS_ERR);
        }
        return Integer.parseInt(choice);
    }

    private void addTaskToSprint() {
        try {
            currentProject.addTaskToSprint(scanInteger());
            monitor.display(currentProject.viewProjectState());
        } catch (IllegalArgumentException e) {
            Utils.logger.warn(Utils.WRONG_ACTION);
            addTaskToSprint();
        }
    }

    public void stopSprint() {
        currentProject.stopSprintOperation();
    }
}
